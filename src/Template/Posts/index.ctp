<div class="row">
    <div class="col-md-3">
        <h3>CRUD CakePHP</h3>
    </div>
    <div class="col-md-6">
        <form action="<?php echo $this->Url->build(['action'=>'search']) ?>" method="get">
            <div class="input-group">
                <input type="search" name="q" class="form-control"/>
                <div class="input-group-prepend">
                    <button class="btn btn-primary input-group-text" type="submit">Search</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-3 text-right">
        <?php echo $this->Html->link('Add Data',['action'=>'add'],['class'=>'btn btn-primary']) ?>
    </div>
</div>
<form method="post">
<p>
    <button type="submit" formaction="<?php echo $this->Url->build(['action'=>'deleteselected']) ?>" class="btn btn-danger">Delete Selected</button>
</p>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th><input type="checkbox" class="selectall"/></th>
            <th>Name</th>
            <th>Detail</th>
            <th width="160">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($posts as $post):
        ?>
        <tr>
            <td><input type="checkbox" class="selectbox" name="ids[]" value="<?php echo $post->id ?>"/></td>
            <td><?php echo $post->name ?></td>
            <td><?php echo $post->detail ?></td>
            <td>
                <?php echo $this->Html->link('Edit', ['action'=>'edit', $post->id], ['class'=>'btn btn-warning']) ?>
                <button type="submit" formaction="<?php echo $this->Url->build(['action'=>'delete', $post->id]) ?>" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
    </tbody>
    <thead>
        <tr>
            <th><input type="checkbox" class="selectall2"/></th>
            <th>Name</th>
            <th>Detail</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
</form>
<?php
$paginator = $this->Paginator->setTemplates([
    'number'=>'<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'current'=>'<li class="page-item active"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'first'=>'<li class="page-item"><a href="{{url}}" class="page-link">&laquo;</a></li>',
    'last'=>'<li class="page-item"><a href="{{url}}" class="page-link">&raquo;</a></li>',
    'prevActive'=>'<li class="page-item"><a href="{{url}}" class="page-link">&lt;</a></li>',
    'nextActive'=>'<li class="page-item"><a href="{{url}}" class="page-link">&gt;</a></li>'
]);
?>
<nav>
    <ul class="pagination">
        <?php
        echo $paginator->first();
        if($paginator->hasPrev()){
            echo $paginator->prev();
        }
        echo $paginator->numbers();
        if($paginator->hasNext()){
            echo $paginator->next();
        }
        echo $paginator->last();
        ?>
    </ul>
</nav>
<script>
$('.selectall').click(function(){
    $('.selectbox').prop('checked',$(this).prop('checked'));
    $('.selectall2').prop('checked',$(this).prop('checked'));
});
$('.selectall2').click(function(){
    $('.selectbox').prop('checked',$(this).prop('checked'));
    $('.selectall').prop('checked',$(this).prop('checked'));
});
$('.selectbox').click(function(){
    var total = $('.selectbox').length;
    var number = $('.selectbox:checked').length;
    if(total == number){
        $('.selectall').prop('checked', true);
        $('.selectall2').prop('checked', true);
    } else{
        $('.selectall').prop('checked', false);
        $('.selectall2').prop('checked', false);
    }
});
</script>