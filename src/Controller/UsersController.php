<?php
namespace App\Controller;

use Cake\Mailer\Email;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;
use Cake\ORM\TableRegistry;

class UsersController extends AppController{

    public function index(){}

    public function forgotpassword(){
        if($this->request->is('post')){
            $myemail = $this->request->getData('email');
            $mytoken = Security::hash(Security::randomBytes(25));

            $userTable = TableRegistry::get('Users');
            $user = $userTable->find('all')->where(['email'=>$myemail])->first();
            $user->password = '';
            $user->token = $mytoken;
            if($userTable->save($user)){
                $this->Flash->success('Reset password link has been sent to your email ('.$myemail.'), please open your inbox');

                Email::configTransport('mailtrap', [
                    'host' => 'smtp.mailtrap.io',
                    'port' => 2525,
                    'username' => '966e315aae5356',
                    'password' => '7dd8fb3e81d9da',
                    'className' => 'Smtp'
                ]);

                $email = new Email('default');
                $email->transport('mailtrap');
                $email->emailFormat('html');
                $email->from('myteukughazali@gmail.com', 'Tedir Ghazali');
                $email->subject('Please confirm your reset password');
                $email->to($myemail);
                $email->send('Hello '.$myemail.'<br/>Please click link below to reset your password<br/><br/><a href="http://localhost:8765/users/resetpassword/'.$mytoken.'">Reset Password</a>');
            }
        }
    }

    public function resetpassword($token){
        if($this->request->is('post')){
            $hasher = new DefaultPasswordHasher();
            $mypass = $hasher->hash($this->request->getData('password'));

            $userTable = TableRegistry::get('Users');
            $user = $userTable->find('all')->where(['token'=>$token])->first();
            $user->password = $mypass;
            if($userTable->save($user)){
                return $this->redirect(['action'=>'login']);
            }
        }
    }

    public function login(){
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else{
                $this->Flash->error('Your username or password is incorrect');
            }
        }
    }

    public function logout(){
        return $this->redirect($this->Auth->logout());
    }

    public function register(){
        if($this->request->is('post')){
            $userTable = TableRegistry::get('Users');
            $user = $userTable->newEntity();

            $hasher = new DefaultPasswordHasher();
            $myname = $this->request->getData('name');
            $myemail = $this->request->getData('email');
            $mypass = $this->request->getData('password'); // will convert to bcrypt hash password
            $mytoken = Security::hash(Security::randomBytes(32));

            $user->name = $myname;
            $user->email = $myemail;
            $user->password = $hasher->hash($mypass);
            $user->token = $mytoken;
            $user->created_at = date('Y-m-d H:i:s');
            $user->updated_at = date('Y-m-d H:i:s');
            if($userTable->save($user)){
                $this->Flash->set('Register successful, your confirmation email has been sent', ['element'=>'success']);

                Email::configTransport('mailtrap', [
                    'host' => 'smtp.mailtrap.io',
                    'port' => 2525,
                    'username' => '966e315aae5356',
                    'password' => '7dd8fb3e81d9da',
                    'className' => 'Smtp'
                ]);

                $email = new Email('default');
                $email->transport('mailtrap');
                $email->emailFormat('html');
                $email->from('myteukughazali@gmail.com', 'Tedir Ghazali');
                $email->subject('Please confirm your email to activation your account');
                $email->to($myemail);
                $email->send('hai '.$myname.'<br/>Please confirm your email link below<br/><a href="http://localhost:8765/users/verification/'.$mytoken.'">Verification Email</a><br/>Thank you for joining us');

            } else{
                $this->Flash->set('Register failed, please try again', ['element'=>'error']);
            }
        }
    }

    public function verification($token){
        $userTable = TableRegistry::get('Users');
        $verify = $userTable->find('all')->where(['token'=>$token])->first();
        $verify->verified = '1';
        $userTable->save($verify);
    }

}